// const express = require('express')
// const asyncify = require('express-asyncify');
// const router = asyncify(express.Router());

import express from 'express';
import asyncify from 'express-asyncify';

const router = asyncify(express.Router());


// Verify JWT with public key
router.get('/parse-resume', async function (req, res) {
    try{
        return res.status(200).send({message: "resume-parse."})
    }catch(err){
        return res.status(401).send({message: err.message})
    }
});

// module.exports = router
export default router;
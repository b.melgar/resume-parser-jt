const AWS = require('aws-sdk');

//Configure AWS credentials
AWS.config.update({
    region: process.env.REGION,
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
  });


var dynamodb = new AWS.DynamoDB();
var docClient = new AWS.DynamoDB.DocumentClient();

  
const RESUME_TABLE_NAME = 'JT-RESUME';


// Universal DynamoDB Functions
const getTable =  async (tableName) => {
    var params = {
      TableName: tableName
    };
    return dynamodb.describeTable(params).promise()
    .then(data => data.Items);
  };
  
  const getItem =  async (tableName, query) => {
    var params = {
      TableName: tableName,
      Key: query
    };
    return docClient.get(params).promise()
    .then(data => data.Item);
  };

  
const getItemSecondary =  async (tableName, indexName, indexKey, indexValue) => {

    // Validate input parameters
    if (!tableName || !indexKey || !indexValue) {
      //todo: should probably use an error enum with the description instead of array
      throw new HttpError("One or more errors occurred.", 400, [5])
    }
  
    const params = {
      TableName: tableName,
      IndexName: indexName,
      KeyConditionExpression: `${indexKey} = :v_index`,
      ExpressionAttributeValues: {
        ":v_index": indexValue
      }
    };
    return docClient.query(params).promise()
    .then(data => data.Items);
  };
  
  const getAllItems = async (tableName) => {
    var params = {
      TableName: tableName,
    }
    return docClient.scan(params).promise()
    .then(data => data.Items);
  }
  
  const createItem = async (tableName, item) => {
    var params = {
      TableName: tableName,
      Item: item
    }
    return docClient.put(params).promise()
    .then(data => params.Item);
  }
  
  const putItem = async (tableName, item) => {
    var params = {
      TableName: tableName,
      Item: item
    }
    return docClient.put(params).promise()
    .then(data => params.Item);
  }
  
  const updateItem = async (tableName, primaryQuery, item) => {
    const source = await getItem(tableName, primaryQuery);
    var params = {
      TableName: tableName,
      Item: Object.assign(source, item)
    }
    return docClient.put(params).promise()
    .then(data => params.Item);
  }
  
  const scan = async (params) => {
    return docClient.scan(params).promise()
    .then(data => data.Items);
  }
  
  const deleteItem = async (tableName, id) => {
    var params = {
      TableName: tableName,
      Key: id
    }
    return docClient.delete(params).promise()
    .then(data => params.Key);
  }
  
  async function executeQuery(statement) {
    const result = await dynamodb.executeStatement({Statement: statement}).promise();
    return result.Items.map(item => {return AWS.DynamoDB.Converter.unmarshall(item)});
  }

  
const deleteTable = async(tableName) => {
    var params = {
      TableName: tableName
    };
    // Call DynamoDB to delete the specified table
    dynamodb.deleteTable(params, function(err, data) {
      if (err && err.code === 'ResourceNotFoundException') {
        console.log("Error: Table not found");
      } else if (err && err.code === 'ResourceInUseException') {
        console.log("Error: Table in use");
      } else {
        console.log("Success: table deleted.", data);
      }
    });
  
  }

  
const initDynamo = async () => {
    // Applications Table
    try{
      var appTable = await getTable(RESUME_TABLE_NAME);
    }catch(err){
      var params = {
        TableName : RESUME_TABLE_NAME,
        KeySchema: [       
            { AttributeName: "resumeId", KeyType: "HASH"},  //Partition key
        ],
        AttributeDefinitions: [       
            { AttributeName: "resumeId", AttributeType: "S" },
        ],
        ProvisionedThroughput: {       
            ReadCapacityUnits: 10, 
            WriteCapacityUnits: 10
        }
      };
      dynamodb.createTable(params).promise()
      .then(data => data)
      .catch(err => {console.log(err)})
    }
}
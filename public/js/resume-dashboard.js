// Sample resume data (replace with your data)
const resumes = [
    { name: "John Doe", category: "developer", skills: "JavaScript, HTML, CSS" },
    { name: "Jane Smith", category: "designer", skills: "UI/UX, Photoshop, Illustrator" },
    { name: "Bob Johnson", category: "manager", skills: "Team Leadership, Management" },
    { name: "Danilo Villaver", category: "developer", skills: "JavaScript, HTML, CSS" },
    { name: "Janely Smithy", category: "designer", skills: "UI/UX, Photoshop, Illustrator" },
    { name: "Bobby Johnson", category: "manager", skills: "Team Leadership, Management" },
    { name: "John Smith", category: "developer", skills: "JavaScript, HTML, CSS" },
    { name: "Jane Johnson", category: "designer", skills: "UI/UX, Photoshop, Illustrator" },
    { name: "Sponge Bob", category: "manager", skills: "Team Leadership, Management" },
    { name: "Johnny Doetay", category: "developer", skills: "JavaScript, HTML, CSS" },
    { name: "Mary Jane", category: "designer", skills: "UI/UX, Photoshop, Illustrator" },
    { name: "Tony Parker", category: "manager", skills: "Team Leadership, Management" },
    { name: "May June", category: "developer", skills: "JavaScript, HTML, CSS" },
    { name: "April Smith", category: "designer", skills: "UI/UX, Photoshop, Illustrator" },
    { name: "Sunday March", category: "manager", skills: "Team Leadership, Management" },
    { name: "John Doe", category: "developer", skills: "JavaScript, HTML, CSS" },
    { name: "Steph Parker", category: "designer", skills: "UI/UX, Photoshop, Illustrator" },
    { name: "John Johnson", category: "manager", skills: "Team Leadership, Management" },
    // Add more resume data here...
];

// Function to display resumes based on filters
function displayResumes() {
    const searchInput = document.getElementById("searchInput").value.toLowerCase();
    const categoryFilter = document.getElementById("categoryFilter").value.toLowerCase();

    const filteredResumes = resumes.filter((resume) => {
        const nameMatches = resume.name.toLowerCase().includes(searchInput);
        const categoryMatches = categoryFilter === "" || resume.category.toLowerCase() === categoryFilter;
        return nameMatches && categoryMatches;
    });

    const resumeList = document.getElementById("resumeList");
    resumeList.innerHTML = ""; // Clear previous results

    if (filteredResumes.length === 0) {
        resumeList.innerHTML = "<p>No matching resumes found.</p>";
    } else {
        filteredResumes.forEach((resume) => {
            const card = document.createElement("div");
            card.className = "resume-card";
            card.innerHTML = `
                <h2>${resume.name}</h2>
                <p>Category: ${resume.category}</p>
                <p>Skills: ${resume.skills}</p>
            `;
            resumeList.appendChild(card);
        });
    }
}

// Event listeners for filter button click and input changes
document.getElementById("filterButton").addEventListener("click", displayResumes);
document.getElementById("searchInput").addEventListener("input", displayResumes);
document.getElementById("categoryFilter").addEventListener("change", displayResumes);

// Initial display of resumes
displayResumes();

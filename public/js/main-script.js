// Get references to HTML elements
const dragDropContainer = document.getElementById('drag-drop-container');
const resumeUpload = document.getElementById('resume-upload');
const uploadButton = document.getElementById('upload-button');
const resumeIframe = document.getElementById('resume-iframe');

// Prevent default behavior for drag-and-drop events
dragDropContainer.addEventListener('dragover', function (e) {
    e.preventDefault();
    dragDropContainer.style.borderColor = '#0056b3';
});

dragDropContainer.addEventListener('dragleave', function (e) {
    e.preventDefault();
    dragDropContainer.style.borderColor = '#007bff';
});

dragDropContainer.addEventListener('drop', function (e) {
    e.preventDefault();
    dragDropContainer.style.borderColor = '#007bff';

    const files = e.dataTransfer.files;
    if (files.length > 0) {
        resumeUpload.files = files;
        previewResume();
    }
});

// Trigger file input click when the drag-and-drop container is clicked
dragDropContainer.addEventListener('click', function () {
    resumeUpload.click();
});

// Listen for changes in the file input
resumeUpload.addEventListener('change', previewResume);

// Function to preview the uploaded resume
function previewResume() {
    const resumeFile = resumeUpload.files[0];

    if (!resumeFile) {
        return;
    }

    // Check if the file type is supported
    if (!isFileTypeSupported(resumeFile)) {
        alert('Unsupported file type. Please upload a PDF or Word document.');
        return;
    }

    // Display the uploaded resume in the iframe
    const resumeUrl = URL.createObjectURL(resumeFile);
    resumeIframe.src = resumeUrl;
}

// Function to check if the file type is supported
function isFileTypeSupported(file) {
    const allowedTypes = ['application/pdf', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'];
    return allowedTypes.includes(file.type);
}
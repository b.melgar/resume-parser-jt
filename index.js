// 'use strict';
import express from 'express';
// const express = require("express")
import bodyParser from 'body-parser';
import cors from 'cors';
import dotenv from 'dotenv';
import cookieParser from 'cookie-parser';
import resume from './routes/resume.js';
const port = process.env.PORT || 3004;
const app = express();

dotenv.config();

app.use(cors({origin: '*'}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static('public'))
app.set('views', 'views');
app.set('view engine', 'ejs');
//testing

app.get('/', (req, res) => {
    res.render('index', { message: 'Hello from EJS!' });
});

app.get('/resume-dashboard', (req, res) => {
    res.render('resume-dashboard', { message: 'Hello from EJS!' });
});

//routes

app.use('/', resume);
app.listen(port);

// module.exports = app;
export default app;

